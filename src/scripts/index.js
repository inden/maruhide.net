$('.page-nav a[href ^= "#"]').on('click', function(event) {
  event.preventDefault();
  var NavHeight = $('nav').height();
  var targetID = $(this).attr('href');
  console.log(targetID);
  var targetST = $(targetID).offset().top - (NavHeight+20);
  $('body, html').animate({
    scrollTop: targetST + 'px'
  }, 300);
});

$('a.gotop').on('click', function(){
  event.preventDefault();
  $('body, html').animate({
    scrollTop: 0
  }, 300, 'swing');
});