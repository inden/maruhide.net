// webpack.config.js
module.exports = {
  entry: {
    index: './src/scripts/index.js',
    'vue-script': './src/scripts/vue-script.js',
  },
  output: {
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      }
    ]
  }
};