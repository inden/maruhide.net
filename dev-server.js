const express = require('express');
const app = express();

app.set('views', `${__dirname  }/src/htmls`);
app.set('view engine', 'pug');

if (app.get('env') === 'development') {
  const browserSync = require('browser-sync');
  const bs = browserSync.create().init({
    logSnippet: false,
    reloadDelay: 500,
    host: '0.0.0.0',
    open: 'local',
    files: [
      `${__dirname  }/src/html_modules/**/*`,
      `${__dirname  }/src/htmls/**/*`,
      `${__dirname  }/tmp/_assets/**/*.js`,
      `${__dirname  }/tmp/_assets/**/*.css`
    ],
  });
  app.use(require('connect-browser-sync')(bs));
}

app.use('/', express.static(`${__dirname  }/src/htmls`));
app.use('/_assets', express.static(`${__dirname  }/tmp/_assets`));

app.get(/\/(.*?)\/$/, (req, res) => {
  // console.log(req.params);
  res.render(`${req.params[0]}/index`);
});

app.get(/\/(.*?).html$/, (req, res) => {
  // console.log(req.params);
  res.render(req.params[0]);
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
